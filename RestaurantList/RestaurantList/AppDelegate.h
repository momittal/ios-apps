//
//  AppDelegate.h
//  RestaurantList
//
//  Created by Mohit Mittal on 3/24/16.
//  Copyright (c) 2016 Mohit Mittal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FactualSDK/FactualAPI.h>
@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    FactualAPI* apiObject;
}
@property (strong, nonatomic) UIWindow *window;
@property (nonatomic,readonly) FactualAPI* apiObject;

+(FactualAPI*) getAPIObject;
+(AppDelegate*) getDelegate;

@end


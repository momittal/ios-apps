//
//  MasterViewController.m
//  RestaurantList
//
//  Created by Mohit Mittal on 3/24/16.
//  Copyright (c) 2016 Mohit Mittal. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"
#import "AppDelegate.h"
@interface MasterViewController ()

@end

@implementation MasterViewController
@synthesize city, restaurant, searchText;
@synthesize queryResult,queryObject, activeRequest, apiObject;
long offset = 0;
BOOL noMoreResults = NO;
 //*apiObject;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    city = [[City alloc]init];
    restaurant = [[Restaurant alloc]init];
    city.restaurantList = [NSMutableArray array];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (city.restaurantList != nil) {
        return [city.restaurantList count];
    }
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Configure the cell...
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView
                             dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleSubtitle
                reuseIdentifier:CellIdentifier];
    }
    restaurant = [city.restaurantList objectAtIndex:indexPath.row];

    NSString *cellLabel = [NSString stringWithFormat:@"%ld. %@",(long)(indexPath.row+1), restaurant.restaurantName];
        NSString *detailLabel = [NSString stringWithFormat:@"%@, %@, %@",restaurant.address, restaurant.rating, restaurant.restaurantType];
        cell.textLabel.text = cellLabel;
        cell.detailTextLabel.text = detailLabel;
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 

    DetailViewController *detailViewController= (DetailViewController *)[segue destinationViewController];

    NSIndexPath *selectedIndexPath =[self.tableView indexPathForSelectedRow];
    restaurant = [city.restaurantList objectAtIndex:selectedIndexPath.row];
    detailViewController.restaurant = restaurant;
    
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *) cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if(activeRequest == nil && indexPath.row == [city.restaurantList count] - 10) {
        
        NSLog(@"Load more");
        offset += 50;
        [self doQuery];
        
    }
}


- (IBAction)onClickOfSearchButton:(id)sender {
    if (city.cityName!=nil){
        [city.restaurantList removeAllObjects];
    }
    
    city.cityName = [searchText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
 
    [self doQuery];


}
//creating query for API request
-(void) doQuery{
    if(city.cityName.length !=0){
        
        
        queryObject = [FactualQuery query];
        [queryObject addRowFilter:[FactualRowFilter fieldName:@"locality" equalTo:city.cityName]];
        queryObject.offset =offset;
        queryObject.limit = 50;
        apiObject = [AppDelegate getAPIObject];
        activeRequest = [apiObject queryTable:@"restaurants-us" optionalQueryParams:queryObject withDelegate:self];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please Enter City Name" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}


//receiving results for query from the API
-(void) requestComplete:(FactualAPIRequest *)request receivedQueryResult:(FactualQueryResult *)queryResultObj {
    if (activeRequest == request && noMoreResults == NO) {
        NSLog(@"Active request Completed with row count:%ld TableId:%@ RequestTableId:%@", (long)[queryResultObj rowCount], queryResultObj.tableId,activeRequest.tableId);
        
        
        self.queryResult = queryResultObj;
        
        
        activeRequest = nil;
        
        
        if ([queryResult.rows count] > 0){
            int i;
            self.navigationItem.title = city.cityName;
            for (i = 0; i < [self.queryResult.rows count]; i++) {
                FactualRow* row = [self.queryResult.rows objectAtIndex:i];
//                NSLog(@"%@, %@, %@, %@",[row valueForName:@"name"], [row valueForName:@"address"], [row valueForName:@"locality"], [row valueForName:@"region"]);
                
                Restaurant *r = [[Restaurant alloc]init];
                r.restaurantName =[row valueForName:@"name"];
                r.restaurantType = [row valueForName:@"attire"];
                r.rating = [row valueForName:@"rating"];
                r.address = [NSString stringWithString:[row valueForName:@"address"]] ;
                [city.restaurantList addObject:r];
            }
            
        }else{
            if([city.restaurantList count] == 0){
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"No Results Found! Please make sure you entered correct city" message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }else{
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"No More Results" message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                noMoreResults = YES;
            }
            
        }
        [self.tableView reloadData];
    }
}
@end

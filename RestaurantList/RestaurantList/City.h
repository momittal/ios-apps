//
//  City.h
//  GetRestaurants
//
//  Created by Mohit Mittal on 3/24/16.
//  Copyright (c) 2016 Mohit Mittal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Restaurant.h"
#import <FactualSDK/FactualQueryResult.h>
#import <FactualSDK/FactualAPI.h>
#import <FactualSDK/FactualQuery.h>

@interface City : NSObject 
{
    NSString * cityName;
    NSMutableArray * restaurantList;
}

@property(strong,retain) NSString * cityName;
@property(strong,retain) NSMutableArray *restaurantList;

@end

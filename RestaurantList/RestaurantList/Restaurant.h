//
//  Restaurant.h
//  GetRestaurants
//
//  Created by Mohit Mittal on 3/24/16.
//  Copyright (c) 2016 Mohit Mittal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Restaurant : NSObject
{
    NSString *restaurantName;
    NSString *address;
    NSString *rating;
    NSString *restaurantType;
}

@property(strong,retain) NSString * restaurantName;
@property(strong,retain) NSString * address;
@property(strong,retain) NSString * rating;
@property(strong,retain) NSString * restaurantType;

@end

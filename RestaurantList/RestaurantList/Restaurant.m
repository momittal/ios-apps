//
//  Restaurant.m
//  GetRestaurants
//
//  Created by Mohit Mittal on 3/24/16.
//  Copyright (c) 2016 Mohit Mittal. All rights reserved.
//

#import "Restaurant.h"

@implementation Restaurant
@synthesize restaurantName;
@synthesize address;
@synthesize rating;
@synthesize restaurantType;

@end

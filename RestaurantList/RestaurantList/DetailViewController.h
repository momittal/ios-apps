//
//  DetailViewController.h
//  RestaurantList
//
//  Created by Mohit Mittal on 3/24/16.
//  Copyright (c) 2016 Mohit Mittal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Restaurant.h"
@interface DetailViewController : UITableViewController
@property (strong, retain) Restaurant *restaurant;
@end

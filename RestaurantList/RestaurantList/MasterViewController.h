//
//  MasterViewController.h
//  RestaurantList
//
//  Created by Mohit Mittal on 3/24/16.
//  Copyright (c) 2016 Mohit Mittal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "City.h"
#import "Restaurant.h"

@interface MasterViewController : UITableViewController <FactualAPIDelegate>
{
    
    FactualAPIRequest* activeRequest;
}
@property (strong, retain) City *city;
@property (weak, nonatomic) IBOutlet UITextField *searchText;
@property (strong, retain) Restaurant *restaurant;

@property (weak, nonatomic) IBOutlet UIButton *searchButton;
- (IBAction)onClickOfSearchButton:(id)sender;
@property (nonatomic,retain)  FactualQueryResult* queryResult;
@property (nonatomic,retain) FactualQuery* queryObject;
@property (strong,nonatomic) FactualAPIRequest *activeRequest;
@property(nonatomic,readonly) FactualAPI *apiObject;
@end
